#!/usr/bin/python

import rospy, csv
import numpy as np
from std_msgs.msg import String
from multi_mav_manager.srv import RawPosFormation, RawPosFormationRequest
from quadrotor_msgs.msg import FlatOutputs
from geometry_msgs.msg import PoseStamped
from mav_manager.srv import Vec4
from mav_manager.srv import Vec4Request

from testcases_upenn import *

class UAV_manager:

    def __init__(self):
        rospy.init_node('uav_manager', anonymous=True)

        # Connect to ROS
        self.sub_1 = rospy.Subscriber('/vicon/QuadrotorUniform/pose', PoseStamped, self.callback_1, queue_size=10)
        self.sub_2 = rospy.Subscriber('/vicon/QuadrotorWhiskey/pose', PoseStamped, self.callback_2, queue_size=10)

        try:
            msg1 = rospy.wait_for_message('/vicon/QuadrotorUniform/pose', PoseStamped, timeout=2)
            msg2 = rospy.wait_for_message('/vicon/QuadrotorWhiskey/pose', PoseStamped, timeout=2)
        except(rospy.ROSException), e:
            print "Service call failed: %s", e
            print "Could not get ODOMETRY"
            #return
        # Read CSV points
        self.traj_folder = rospy.get_param("~trajectory_path")
        self.traj_1 = np.loadtxt(self.traj_folder + ('traj_robot_1.csv'), delimiter=',')
        self.traj_2 = np.loadtxt(self.traj_folder + ('traj_robot_2.csv'), delimiter=',')
        # print traj_1

        # TODO Uncomment this when running on real UAVS for safety
        # Check to make sure the UAV is close enough to the first waypoint
        first_pt_1 = self.traj_1[0, :]
        first_pt_2 = self.traj_2[0, :]
        # dist1 = (msg1.pose.position.x - first_pt_1[0]) ** 2 + (msg1.pose.position.y - first_pt_1[1]) ** 2
        # dist2 = (msg2.pose.position.x - first_pt_2[0]) ** 2 + (msg2.pose.position.y - first_pt_2[1]) ** 2
        # if dist1 > 1.0 or dist2 > 1.0:
        #     print "Dist1: ", dist1, " Dist2: ", dist2, "Robots are not in safe starting position.\n"
        #     return

        # Init curr positions
        self.curr_x_1 = []
        self.curr_y_1 = []
        self.curr_z_1 = []
        self.curr_x_2 = []
        self.curr_y_2 = []
        self.curr_z_2 = []


    def callback_1(self, data):
        self.curr_x_1 = data.pose.position.x
        self.curr_y_1 = data.pose.position.y
        self.curr_z_1 = data.pose.position.z

    def callback_2(self, data):
        self.curr_x_1 = data.pose.position.x
        self.curr_y_1 = data.pose.position.y
        self.curr_z_1 = data.pose.position.z

    def main(self):
        # TODO Uncomment these for safety when using Real UAVs
        # rospy.wait_for_service('/QuadrotorUniform/mav_services/goTo')
        # rospy.wait_for_service('/QuadrotorWhiskey/mav_services/goTo')

        rate = rospy.Rate(2.5)  # Waypoints sent at 2 Hz
        wp_index = 0
        robotsTrajectory = motionPlanning_test2()
        traj0_X = robotsTrajectory[0]['x']
        traj0_Y = robotsTrajectory[0]['y']

        traj1_X = robotsTrajectory[1]['x']
        traj1_Y = robotsTrajectory[1]['y']

        while not rospy.is_shutdown() and wp_index < self.traj_1.shape[0]:
            # Read waypoints from the CSV file.

            x_1 = traj0_X[wp_index]
            y_1 = traj0_Y[wp_index]
            z_1 = 1
            yaw_1 = 0

            x_2 = traj1_X[wp_index]
            y_2 = traj1_Y[wp_index]
            z_2 = 1
            yaw_2 = 0

            # x_1 = self.traj_1[wp_index, 0]
            # y_1 = self.traj_1[wp_index, 1]
            # z_1 = self.traj_1[wp_index, 2]
            # yaw_1 = self.traj_1[wp_index, 3]
            #
            # x_2 = self.traj_2[wp_index, 0]
            # y_2 = self.traj_2[wp_index, 1]
            # z_2 = self.traj_2[wp_index, 2]
            # yaw_2 = self.traj_2[wp_index, 3]

            # TODO Write the waypoints, X,Y,Z, Yaw into wp_1 and wp_2 respectively.
            wp_1 = [x_1, y_1, z_1, yaw_1]
            wp_2 = [x_2, y_2, z_2, yaw_2]

            # Create request(s)
            req_1 = Vec4Request()
            req_1.goal = wp_1

            req_2 = Vec4Request()
            req_2.goal = wp_2

            print 'Waypoint_1', wp_index, ': ', wp_1
            print 'Waypoint_2', wp_index, ': ', wp_2
            # Try to send the waypoint
            try:
                goToClient_1 = rospy.ServiceProxy('/QuadrotorUniform/mav_services/goTo', Vec4)
                response_1 = goToClient_1(req_1)
                print 'Response received: ', response_1

                goToClient_2 = rospy.ServiceProxy('/QuadrotorWhiskey/mav_services/goTo', Vec4)
                response_2 = goToClient_2(req_2)
                print 'Response received: ', response_2
                wp_index += 1  # Increment waypoint counter

            except rospy.ServiceException, e:
                print "Service call failed: %s", e

            rate.sleep()


if __name__ == '__main__':
    try:
        uav_mgr = UAV_manager()
        uav_mgr.main()
    except rospy.ROSInterruptException:
        pass
