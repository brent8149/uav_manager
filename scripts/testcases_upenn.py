#* --------------------------------------------------------------------------
#  * File: Testcases.py
#  * ---------------------------------------------------------------------------
#  * Copyright (c) 2016 The Regents of the University of California.
#  * All rights reserved.
#  *
#  * Redistribution and use in source and binary forms, with or without
#  * modification, are permitted provided that the following conditions
#  * are met:
#  * 1. Redistributions of source code must retain the above copyright
#  *    notice, this list of conditions and the following disclaimer.
#  * 2. Redistributions in binary form must reproduce the above
#  *    copyright notice, this list of conditions and the following
#  *    disclaimer in the documentation and/or other materials provided
#  *    with the distribution.
#  * 3. All advertising materials mentioning features or use of this
#  *    software must display the following acknowledgement:
#  *       This product includes software developed by Cyber-Physical
#  *       Systems Lab at UCLA and UC Berkeley.
#  * 4. Neither the name of the University nor that of the Laboratory
#  *    may be used to endorse or promote products derived from this
#  *    software without specific prior written permission.
#  *
#  * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS''
#  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
#  * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
#  * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS
#  * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
#  * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#  * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
#  * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
#  * SUCH DAMAGE.
#  *
#  * Developed by: Yasser Shoukry
#  */

import csv
from itertools import izip

from MultiRobotMotionPlanner_upenn import *



# ***************************************************************************************************
# ***************************************************************************************************
#
#         TEST2: TWO ROBOTS IN A CROSS
#
# ***************************************************************************************************
# ***************************************************************************************************

def motionPlanning_test2():
    numberOfRobots          = 2
    safetyLimit             = 1.0
    dwell                   = 7
    inputLimit              = 0.5
    maxHorizon              = 1000
    Ts                      = 0.5
    numberOfIntegrators     = 4

    maxVelocity             = 1
    maxAcceleration         = 2.5

    A_square                = np.array([[-1.0, 0.0],
                                        [1.0, 0.0],
                                        [0.0, -1.0],
                                        [0.0, 1.0]])

    regionCorners           = []
    #   2   5   8
    #   1   4   7
    #   0   3   6
    regionCorners.append({'xmin': 0.0, 'xmax': 1.0, 'ymin': -2.0, 'ymax': 0.0})      # region 0
    regionCorners.append({'xmin': 0.0, 'xmax': 1.0, 'ymin': 0.0, 'ymax': 1.0})      # region 1
    regionCorners.append({'xmin': 0.0, 'xmax': 1.0, 'ymin': 1.0, 'ymax': 2.0})      # region 2

    regionCorners.append({'xmin': 1.0, 'xmax': 3.0, 'ymin': -2.0, 'ymax': 0.0})      # region 3
    regionCorners.append({'xmin': 1.0, 'xmax': 3.0, 'ymin': 0.0, 'ymax': 1.0})      # region 4
    regionCorners.append({'xmin': 1.0, 'xmax': 3.0, 'ymin': 1.0, 'ymax': 2.0})      # region 5

    regionCorners.append({'xmin': 3.0, 'xmax': 5.0, 'ymin': -2.0, 'ymax': 0.0})      # region 6
    regionCorners.append({'xmin': 3.0, 'xmax': 5.0, 'ymin': 0.0, 'ymax': 1.0})      # region 7
    regionCorners.append({'xmin': 3.0, 'xmax': 5.0, 'ymin': 1.0, 'ymax': 2.0})      # region 8

    adjacents                = []
    adjacents.append([0, 1, 3])
    adjacents.append([1, 0, 2, 4])
    adjacents.append([2, 1, 5])
    adjacents.append([3, 0, 4, 6])
    adjacents.append([4, 1, 3, 5, 7])
    adjacents.append([5, 2, 4, 8])
    adjacents.append([6, 3, 7])
    adjacents.append([7, 4, 6, 8])
    adjacents.append([8, 5, 7])

    regions = []
    obstacleRegions         = [True,False,True,False,False,False,True,False,True]
    for counter in range(0,9):
        b = np.array([-1 * regionCorners[counter]['xmin'], regionCorners[counter]['xmax'],
                      -1 * regionCorners[counter]['ymin'], regionCorners[counter]['ymax']])
        regions.append({'A': A_square, 'b':b, 'isObstacle': obstacleRegions[counter], 'adjacents':adjacents[counter]})

    workspace = {'xmin': 0.0, 'xmax': 5.0, 'ymin': 0.0, 'ymax': 5.0, 'regions':regions}

    robotsInitialState      = []
    robotsInitialState.append({'x0': 0.0, 'y0': 0.5, 'region':1})   # first robot starts at (2.5,0)
    robotsInitialState.append({'x0': 5.0, 'y0': 0.5, 'region':7})   # second robot starts at (0,2.5)
    #robotsInitialState.append({'x0': 2.0, 'y0': 5.0, 'region':5})   # second robot starts at (0,2.5)
    #robotsInitialState.append({'x0': 2.0, 'y0': 0.0, 'region':3})  # second robot starts at (0,2.5)


    robotsGoalState          = []
    robotsGoalState.append({'xf': 5.0, 'yf': 0.5, 'region':7})      # first robot goal is (2.5, 5.0)
    robotsGoalState.append({'xf': 0.0, 'yf': 0.5, 'region':1})      # second robot goal is (2.5, 5.0)
    #robotsGoalState.append({'xf': 2.0, 'yf': 0.0, 'region':3})  # second robot goal is (2.5, 5.0)
    #robotsGoalState.append({'xf': 2.0, 'yf': 5.0, 'region':5})  # second robot goal is (2.5, 5.0)

    inputConstraints        = []
    inputConstraints.append({'uxMax': inputLimit, 'uxMin': -1 * inputLimit,
                             'uyMax': inputLimit, 'uyMin': -1 * inputLimit})   # input constraints for the first robot
    inputConstraints.append({'uxMax': inputLimit, 'uxMin': -1 * inputLimit,
                             'uyMax': inputLimit, 'uyMin': -1 * inputLimit})    # input constraints for the second robot
    inputConstraints.append({'uxMax': inputLimit, 'uxMin': -1 * inputLimit,
                             'uyMax': inputLimit, 'uyMin': -1 * inputLimit})    # input constraints for the second robot
    inputConstraints.append({'uxMax': inputLimit, 'uxMin': -1 * inputLimit,
                             'uyMax': inputLimit, 'uyMin': -1 * inputLimit})    # input constraints for the second robot


    start                   = timeit.default_timer()
    for horizon in range(3,maxHorizon):
        print '\n=============================================='
        print '         Horizon = ', horizon
        print '==============================================\n'
        solver = MultiRobotMotionPlanner(horizon, numberOfRobots, workspace, numberOfIntegrators, maxVelocity, maxAcceleration)
        robotsTrajectory, loopIndex, counter_examples = solver.solve(
            robotsInitialState, robotsGoalState, inputConstraints, Ts, safetyLimit, dwell
        )
        if robotsTrajectory:
            break


    #end = timeit.default_timer()
    #time_smt = end - start
    #print 'Exuection time = ', time_smt
    #print 'Number of Robots = ', numberOfRobots
    #print 'Safety Limit = ', safetyLimit
    #print 'Trajectory length = ', len(robotsTrajectory[0]['x'])

    #print 'robotsTrajectory', robotsTrajectory

    return  robotsTrajectory


    '''
    limit       = len(robotsTrajectory[0]['x'])
    with open('traj_robot_1.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(izip(
            robotsTrajectory[0]['x'][0:limit],
            robotsTrajectory[0]['y'][0:limit],
            [1.0] * limit,
            [0.0] * limit
        ))

    with open('traj_robot_2.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(izip(
            robotsTrajectory[1]['x'][0:limit],
            robotsTrajectory[1]['y'][0:limit],
            [1.0] * limit,
            [0.0] * limit
        ))


    __animateTrajectories(robotsTrajectory, loopIndex, safetyLimit, workspace)
    '''


# ***************************************************************************************************
# ***************************************************************************************************
#
#         Animate Trajectories
#
# ***************************************************************************************************
# ***************************************************************************************************

def __animateTrajectories(robotsTrajectory, loopIndex, safetyLimit, workspace):
    numberOfRobots      = len(robotsTrajectory)
    colors              = np.random.random((numberOfRobots, 3))



    if loopIndex > 0:
        numberOfLoops = 5
        loopPoints = range(loopIndex + 1, len(robotsTrajectory[0]['x']))

        for loop in range(0, numberOfLoops):
            for robotIndex in range(0, numberOfRobots):
                robotsTrajectory[robotIndex]['x'] += [robotsTrajectory[robotIndex]['x'][i] for i in loopPoints]
                robotsTrajectory[robotIndex]['y'] += [robotsTrajectory[robotIndex]['y'][i] for i in loopPoints]


    # animate the trajectory
    fig                 = plt.figure(figsize=(7, 7))
    titleText           = '# Robots = %d, safety limit = %s' % (numberOfRobots, safetyLimit)
    plt.title(titleText)

    ax = fig.add_subplot(111, autoscale_on=False, xlim=(workspace['xmin'], workspace['xmax']), ylim=(workspace['ymin']+2, workspace['ymax']+2))
    ax.grid()
    ax.set_xlim(workspace['xmin'], workspace['xmax']), ax.set_xticks([])
    ax.set_ylim(workspace['ymin']+2, workspace['ymax']+2), ax.set_yticks([])

    def animationUpdate(framenumber):
        thisx               = []
        thisy               = []

        ax.clear()
        for region in workspace['regions']:
            if region['isObstacle']:
                xmin    = -1*region['b'][0]
                xmax    = region['b'][1]
                ymin    = -1*region['b'][2] +2
                ymax    = region['b'][3] + 2
                height  = ymax - ymin
                width   = xmax - xmin

                ax.add_patch(
                    patches.Rectangle(
                        (xmin, ymin),  # (x,y)
                        width,  # width
                        height,  # height
                )
        )

        for robotIndex in range(0, numberOfRobots):
            thisx.append(robotsTrajectory[robotIndex]['x'][framenumber])
            thisy.append(robotsTrajectory[robotIndex]['y'][framenumber]+2)

        ax.scatter(thisx, thisy, c=colors, s=200)
        ax.set_xlim(workspace['xmin'], workspace['xmax']), ax.set_xticks([])
        ax.set_ylim(workspace['ymin'], workspace['ymax']), ax.set_yticks([])

    animation = FuncAnimation(fig, animationUpdate, np.arange(1, len(robotsTrajectory[0]['x'])),interval=50)
    plt.show()




# ***************************************************************************************************
# ***************************************************************************************************
#
#         MAIN FUNCTION
#
# ***************************************************************************************************
# ***************************************************************************************************


if __name__ == "__main__":
    np.random.seed(0)

    #motionPlanning_test62()
    motionPlanning_test2()

    #motionPlanning_test7(2)
    #for numberOfIntegrators in range(2,7):
    #    motionPlanning_test7(numberOfIntegrators)


