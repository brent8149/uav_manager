# UAV Manager

# Install Instructions
## Install ROS, and set up a catkin workspace: 

http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment

## Clone this repository into the catkin workspace

`git clone https://bitbucket.org/brent8149/uav_manager/src`

## Build the uav_manager node

`cd ~/catkin_ws`

`catkin build uav_manager`

## To send waypoints to the UAVs, run:

`roslaunch uav_manager uav_manager.launch`